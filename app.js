var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var irc = require('irc');
var express = require("express");
app.use(express.static(__dirname));

app.get('/', function (req, res) {
	res.sendFile('/index.html');
	res.send('Servidor ativo.');
});

/*

 Descrição: Trabalho do grupo 1 da disciplina de aplicações distribuídas de 2018/2;
 Informação: Para a base da conexão inicial utilizamos a lógica do código inicial do projeto (e modulos do nodejs) disponível em: https://gitlab.com/ad-si-2017-1/p2-g4/
             criado por Weliton Marcos para gerar nossa primeira versão, porém o servidor, funções, frontend, e funcionalidades são distintas de nosso projeto.

*/

io.on("connection", function (client) {

	client.on("join", function (name, channel) {

		var clientIrc = new irc.Client('irc.freenode.net', name, {
			autoConnect: false

		});

		clientIrc.connect(5, function (name) {
			console.log(name + " conectado.");
			clientIrc.join(channel, function (name) {
			clientIrc.say("Agora falando no canal #"+channel);
			});

		});

		clientIrc.addListener('error', function (message) {
			console.log("Erro");
			console.log(message);
		});

		clientIrc.addListener('who', function (who){
			client.emit('chat',who);
		});

		clientIrc.addListener('motd', function (motd) {
			console.log(motd);
			client.emit('chat', motd);
		});

		// Lista de nicks que estão no canal / servidor
		clientIrc.addListener('names', function (channel, nicks) {
			client.emit("update", "", nicks);
			console.log("Usuários no canal " + channel + ":");
			console.log(nicks);

		});
		/**Emitido quando um usuário se junta a um canal (inclusive quando o próprio cliente se junta a um canal) */
		clientIrc.addListener('join', function (channel, nick) {
			client.emit("update", nick);
		});

		clientIrc.addListener('topic', function (data) {
			client.emit('chat', data);
		});

		clientIrc.addListener('quit', function(message){
			client.emit('chat', message);
			client.emit("update", nick);
		})

		clientIrc.addListener('nick', function(oldnick, newnick){
			client.emit('chat', oldnick +" alterou o nick para "+ newnick);
		})

	//	clientIrc.addListener('time',function(time){
	//		console.log(time);
	//	})

		clientIrc.addListener('message', function (from, to, text) {
			console.log(from + ' => ' + to + ': ' + text);
			console.log("Message: " + text);
			client.emit('chat', from, text);
		});

		client.on("send", function (msg) {
			var mensagemCompleta = String(msg);
			var mensagem = String(msg).trim();

			var args = mensagem.split(" ");
			var comando=args[0].toUpperCase();

			// Verifica o comando digitado
			if (comando === "/NICK") 
				nick(args);
		    else if (comando === "/NAMES") // Comando: /names <#Canal>
				names(args);
			else if (comando === "/KICK")  // Comando: /kick <#Canal> <Nick> Obs: Necessita ser OPerador;
				kick(args);
			//else if (comando === "/TIME")
		      //time();
			//else if (comando === "/WHO")
			  //clientIrc.send('who',args[1]);
			else if (comando === "/PART")
			  part(args);
			else if (comando === "/QUIT")
			  quit(args);
			else if(comando === "/MOTD")
			  motd();
			else if(comando === "/TOPIC")
			  topic(args);
			else {
				clientIrc.say(channel, msg);
			}
		});

	// Funções

	// Nick -- /nick <NovoNick> Autor: @jeanmatsunaga
	function nick(args) {
		clientIrc.send('nick', args[1]);
    }
	// Names -- //names <#Canal> Autor: @jeanmatsunaga
	function names(args) {
		clientIrc.send('names',args[1]);
	}
   // Kick -- /kick <#Canal> <Nick> @Necessita ser OPerador do canal; Autor: @jeanmatsunaga
	function kick(args){
		clientIrc.send('kick',args[1], args[2]);
	}

	// Part -- /part <#Canal> Utilizado para sair de um canal; Autor: @jeanmatsunaga
	function part(args){
		clientIrc.send('part',args[1]);
	}

	// Quit -- /quit <Mensagem> Utilizado para sair do servidor; Autor: @jeanmatsunaga
	function quit(args){
		clientIrc.send('quit',args[1]);
	}

	// Motd -- /motd Utilizado para listar a mensagem do dia; Autor: @jeanmatsunaga
	function motd(){
		clientIrc.send('motd');
	}

	// Topic -- /topic <#Canal> Utilizado para listar o topico do canal. Autor: @jeanmatsunaga
	function topic(args){
		clientIrc.send('topic',args[1]);
	}

	});
	
	
});

http.listen(3000, function () {
	console.log('O servidor está sendo executado em: http://localhost:3000');
});
