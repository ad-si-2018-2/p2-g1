<script> const messages = document.getElementById('chatlogs');

function appendMessage() {
	const message = document.getElementsByClassName('chat-message')[0];
  const newMessage = message.cloneNode(true);
  messages.appendChild(newMessage);
}

function getMessages() {
	// Prior to getting your messages.
  shouldScroll = chatlogs.scrollTop + chatlogs.clientHeight === chatlogs.scrollHeight;
  /*
   * Get your messages, we'll just simulate it by appending a new one syncronously.
   */
  appendMessage();
  // After getting your messages.
  if (!shouldScroll) {
    scrollToBottom();
  }
}

function scrollToBottom() {
  chatlogs.scrollTop = chatlogs.scrollHeight;
}

scrollToBottom();

setInterval(getMessages, 100);

</script>