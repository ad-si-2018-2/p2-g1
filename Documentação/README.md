# Diagramas de Sequência
## Comandos do IRC

**Data** | **Descrição** | **Autor**
:---:|:---------:|:-------:
15/11/18 | Criação e Inserção de diagramas de sequência | Victor Murilo

## Sumário

* [Mensagens Suportadas]
* [Mensagens Não Suportadas]
* [Diagramas]

***

### 2. Mensagens Suportadas e seus Diagramas

* Nick (/nick [nickname])
    * Solicita a modificação do nickname, sendo alterado caso não haja conflito.
    * 
    ![Alt Text](https://gitlab.com/ad-si-2018-2/p2-g1/raw/victorbranch/Documenta%C3%A7%C3%A3o/NICK.png)

* Part (/part #canal [mensagem])
    * Faz sair do canal determinado (a mensagem é opcional).
    * 
    ![Alt Text](https://gitlab.com/ad-si-2018-2/p2-g1/raw/victorbranch/Documenta%C3%A7%C3%A3o/PART.png)
    
* MOTD (/motd #canal [mensagem])
    * Cria ou altera mensagem do dia (caso seja administrador) e Exibe mensagem do dia (caso seja usuário).
    * 
    ![Alt Text](https://gitlab.com/ad-si-2018-2/p2-g1/raw/victorbranch/Documenta%C3%A7%C3%A3o/MOTD.png)
    
* Topic (/topic #canal novotopic)
    * Insere um tópico no canal ou modifica um pré-existente.
    * 
    ![Alt Text](https://gitlab.com/ad-si-2018-2/p2-g1/raw/victorbranch/Documenta%C3%A7%C3%A3o/TOPIC.png)
    
* Names (/names #canal)
    * Este comando lista os nomes ativos no canal.
    * 
    ![Alt Text](https://gitlab.com/ad-si-2018-2/p2-g1/raw/victorbranch/Documenta%C3%A7%C3%A3o/NAMES.png)
    
* Quit (/quit [mensagem])
    * Fecha a conexão com o servidor, similar o PART porém fechando todos os canais.
    * 
    ![Alt Text](https://gitlab.com/ad-si-2018-2/p2-g1/raw/victorbranch/Documenta%C3%A7%C3%A3o/QUIT.png)
    
* Kick (/kick #canal nick)
    * Remove o usuário "nick"
    * 
    ![Alt Text](https://gitlab.com/ad-si-2018-2/p2-g1/raw/victorbranch/Documenta%C3%A7%C3%A3o/KICK.png)

***

### 3. Mensagens não Suportadas

* Invite
* Partall
* Notice e seus derivados
* Action e seus derivados
* Query
* Who e seus derivados
* Away

***